package com.epam.algorithm;

import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MergeSortTest {

  private final int[] inputArray = {
      90, 23, 101
  };
  private final int[] sortedArray = Arrays.stream(inputArray).sorted().toArray();

  @Test
  void shouldReturnHappyPath() {
    MergeSort mergeSort = new MergeSort();
    Assertions.assertArrayEquals(mergeSort.sort(inputArray, 0, inputArray.length - 1),sortedArray );
  }
}
