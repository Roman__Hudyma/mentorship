package com.epam.algorithm;

import static com.epam.algorithm.BinarySearch.searchIteratively;
import static com.epam.algorithm.BinarySearch.searchRecursively;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BinarySearchTest {

  private final int[] testArray = {3, 2, 4, 40, 10, 45, 51};
  private final int[] sortedTestArray = {2, 3, 4, 10, 40, 45, 51};

  @Test
  void shouldReturnNeededValueByIterativeBinarySearch() {
    Assertions.assertEquals(4, searchIteratively(sortedTestArray, 40));
  }

  @Test
  void shouldReturnNeededValueByRecursivelySearch() {
    Assertions.assertEquals(3, searchRecursively(sortedTestArray, 0, sortedTestArray.length, 10));
  }

  @Test
  void shouldReturnNeededValueByBinarySearchFromUnsortedArray() {
    Assertions.assertEquals(4, searchIteratively(testArray, 40));
  }
}
