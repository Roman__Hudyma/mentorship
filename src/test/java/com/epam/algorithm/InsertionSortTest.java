package com.epam.algorithm;

import static com.epam.algorithm.InsertionSort.sortByInsertion;

import java.util.Arrays;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ValueSource;

public class InsertionSortTest {

  private final int[] inputArray = {
      90, 23, 101
  };
  private final int[] sortedArray = Arrays.stream(inputArray).sorted().toArray();

  @Test
  void shouldReturnHappyPath() {
    Assertions.assertArrayEquals(sortByInsertion(inputArray), sortedArray);
  }

  @ParameterizedTest
  
  void shouldReturnHappyPathParametrized() {
    Assertions.assertArrayEquals(sortByInsertion(inputArray), sortedArray);
  }
  private static Stream<Arguments> provideStringsForIsBlank() {
    return Stream.of(
        Arguments.of(),
        Arguments.of("")
    );
  }
}
