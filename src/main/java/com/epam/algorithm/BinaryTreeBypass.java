package com.epam.algorithm;

public class BinaryTreeBypass {

  static class Node {

    int key;
    Node left, right;

    public Node(int data) {
      key = data;
      left = right = null;
    }
  }

  Node root;

  BinaryTreeBypass() {
    root = null;
  }

  void deleteKey(int key) {
    root = deleteRecursive(root, key);
  }

  Node deleteRecursive(Node root, int key) {
    if (root == null) {
      return root;
    }

    if (key < root.key) {
      root.left = deleteRecursive(root.left, key);
    } else if (key > root.key) {
      root.right = deleteRecursive(root.right, key);
    } else {
      if (root.left == null) {
        return root.right;
      } else if (root.right == null) {
        return root.left;
      }
      root.key = minValue(root.right);

      root.right = deleteRecursive(root.right, root.key);
    }
    return root;
  }

  int minValue(Node root) {
    int minval = root.key;
    while (root.left != null) {
      minval = root.left.key;
      root = root.left;
    }
    return minval;
  }

  void insert(int key) {
    root = insertRecursive(root, key);
  }

  Node insertRecursive(Node root, int key) {
    if (root == null) {
      root = new Node(key);
      return root;
    }
    if (key < root.key) {
      root.left = insertRecursive(root.left, key);
    } else if (key > root.key) {
      root.right = insertRecursive(root.right, key);
    }
    return root;
  }

  void inorder() {
    inorderRecursive(root);
  }

  void inorderRecursive(Node root) {
    if (root != null) {
      inorderRecursive(root.left);
      System.out.print(root.key + " ");
      inorderRecursive(root.right);
    }
  }

  boolean search(int key) {
    root = searchRecursive(root, key);
    if (root != null) {
      return true;
    } else {
      return false;
    }
  }

  Node searchRecursive(Node root, int key) {
    if (root == null || root.key == key) {
      return root;
    }
    if (root.key > key) {
      return searchRecursive(root.left, key);
    }
    return searchRecursive(root.right, key);
  }
}
