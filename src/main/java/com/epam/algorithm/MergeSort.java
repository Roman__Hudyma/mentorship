package com.epam.algorithm;

class MergeSort {

  void merge(int[] arr, int beg, int mid, int end) {

    int l = mid - beg + 1;
    int r = end - mid;

    int[] leftArray = new int[l];
    int[] rightArray = new int[r];

    if (l >= 0) {
      System.arraycopy(arr, beg, leftArray, 0, l);
    }

    for (int j = 0; j < r; ++j) {
      rightArray[j] = arr[mid + 1 + j];
    }

    int i = 0, j = 0;
    int k = beg;
    while (i < l && j < r) {
      if (leftArray[i] <= rightArray[j]) {
        arr[k] = leftArray[i];
        i++;
      } else {
        arr[k] = rightArray[j];
        j++;
      }
      k++;
    }
    while (i < l) {
      arr[k] = leftArray[i];
      i++;
      k++;
    }

    while (j < r) {
      arr[k] = rightArray[j];
      j++;
      k++;
    }
  }

  int[] sort(int[] arr, int beg, int end) {
    if (beg < end) {
      int mid = (beg + end) / 2;
      sort(arr, beg, mid);
      sort(arr, mid + 1, end);
      merge(arr, beg, mid, end);
    }
    return arr;
  }
}
