package com.epam.algorithm;

import static com.epam.algorithm.InsertionSort.sortByInsertion;

class BinarySearch {

  public static int searchIteratively(int[] input, int number) {
    sortByInsertion(input);
    int first = 0;
    int last = input.length - 1;
    int middle = (first + last) / 2;

    while (first <= last) {
      if (input[middle] < number) {
        first = middle + 1;
      } else if (input[middle] == number) {
        System.out.printf(number + " found at location %d %n", middle);
        return middle;
      } else {
        last = middle - 1;
      }
      middle = (first + last) / 2;
    }
    return -1;
  }

  public static int searchRecursively(int[] sortedArray, int begin, int end, int key) {
    if (begin < end) {
      int middle = begin + (end - begin) / 2;
      if (key < sortedArray[middle]) {
        return searchRecursively(sortedArray, begin, middle, key);
      } else if (key > sortedArray[middle]) {
        return searchRecursively(sortedArray, middle + 1, end, key);
      } else {
        return middle;
      }
    }
    return -1;
  }
}
