package rest.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rest.dto.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

  List<Event> findByTitle(String title);
}
