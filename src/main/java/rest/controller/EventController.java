package rest.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import rest.dto.Event;
import rest.service.EventService;

@RestController
public class EventController {

  @Autowired
  EventService eventService;

  @GetMapping("/events")
  List<Event> getAll() {
    return eventService.getAllEvents();
  }

  @GetMapping("/events/{id}")
  Event getById(@PathVariable Long id) {
    return eventService.getEventById(id);
  }

  @PostMapping("/events")
  void createEvent(@RequestBody Event event) {
    eventService.createEvent(event);
  }

  @PutMapping("/events/{id}")
  void updateEvent(@RequestBody Event event, @PathVariable Long id) {
    Optional<Event> eventOptional = Optional.ofNullable(eventService.getEventById(id));
    eventService.createEvent(event);
    if (eventOptional.isPresent()) {
      event.setId(id);
      eventService.updateEvent(event);
    }
  }

  @DeleteMapping("events/{id}")
  void deleteEvent(@PathVariable Long id) {
    eventService.delete(id);
  }

}
