package rest.service;

import java.util.List;
import org.springframework.stereotype.Service;
import rest.dto.Event;

public interface EventService {

  void createEvent(Event event);

  void updateEvent(Event event);

  void delete(Long id);

  Event getEventById(Long id);

  List<Event> getAllEvents();

  List<Event> getAllEventsByTitle(String title);

}
