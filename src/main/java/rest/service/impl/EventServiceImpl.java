package rest.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.dto.Event;
import rest.repository.EventRepository;
import rest.service.EventService;

@Service
public class EventServiceImpl implements EventService {

  @Autowired
  EventRepository repository;
  @Override
  public void createEvent(Event event) {
    repository.save(event);
  }

  @Override
  public void updateEvent(Event event) {
    repository.save(event);
  }

  @Override
  public void delete(Long id) {
    repository.deleteById(id);
  }

  @Override
  public Event getEventById(Long id) {
    return repository.findById(id).get();
  }

  @Override
  public List<Event> getAllEvents() {
    return repository.findAll();
  }

  @Override
  public List<Event> getAllEventsByTitle(String title) {
    return repository.findByTitle(title);
  }
}
