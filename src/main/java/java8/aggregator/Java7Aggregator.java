package java8.aggregator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javafx.util.Pair;

public class Java7Aggregator implements Aggregator {

  @Override
  public int sum(List<Integer> numbers) {
    int sum = 0;
    for (int x : numbers) {
      sum += x;
    }
    return sum;
  }

  @Override
  public List<Pair<String, Long>> getMostFrequentWords(List<String> words, long limit) {
    Map<String, Integer> wordsMap = new HashMap<String, Integer>();
    List<Pair<String, Long>> frequencies = new ArrayList<>();
    for (String word : words) {
      Integer count = wordsMap.get(word);
      wordsMap.put(word, (count == null) ? 1 : count + 1);
    }
    for (Entry<String,Integer>entry:wordsMap.entrySet()) {
      frequencies.add(new Pair(entry.getKey(),entry.getValue()));
    }
    return frequencies;
  }

  @Override
  public List<String> getDuplicates(List<String> words, long limit) {
    List<String> duplicates = new ArrayList<>();
    Set<String> uniques = new HashSet<>();
    words.replaceAll(String::toUpperCase);
    for (String word : words) {
      if (!uniques.add(word)) {
        duplicates.add(word);
      }
    }
    duplicates.sort(new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {
        if (o1.length() > o2.length()) {
          return 1;
        } else if (o1.length() < o2.length()) {
          return -1;
        }
        return o1.compareTo(o2);
      }
    });
    return duplicates;
  }
}
