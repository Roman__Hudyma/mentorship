package java8.aggregator;

import java.util.List;
import java.util.concurrent.RecursiveTask;
import javafx.util.Pair;

public class Java7ParallelAggregator implements Aggregator {

    @Override
    public int sum(List<Integer> numbers) {
        class SumNumbers extends RecursiveTask<Integer>
        {
            @Override
            protected Integer compute() {
                int sum=0;
                for(Integer integer:numbers)
                {
                    sum+=integer;
                }
                return sum;
            }
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Pair<String, Long>> getMostFrequentWords(List<String> words, long limit) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> getDuplicates(List<String> words, long limit) {
        throw new UnsupportedOperationException();
    }
}
