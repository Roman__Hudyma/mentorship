package java8.aggregator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class Java8Aggregator implements Aggregator {

  @Override
  public int sum(List<Integer> numbers) {
    return numbers.stream().mapToInt(Integer::intValue).sum();
  }

  @Override
  public List<Pair<String, Long>> getMostFrequentWords(List<String> words, long limit) {
    List<Pair<String, Long>> mostFrequentWords = new ArrayList<>();
    Map<String, Long> wordsMap = words.stream()
        .collect(Collectors.groupingBy(word -> word, Collectors.summingLong(word -> 1)));
    for (Map.Entry<String, Long> entry : wordsMap.entrySet()) {
      mostFrequentWords.add(new Pair(entry.getKey(), entry.getValue()));
    }
    return mostFrequentWords;
  }

  @Override
  public List<String> getDuplicates(List<String> words, long limit) {
    return words.stream().map(String::toUpperCase)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
        .entrySet().stream()
        .filter(p -> p.getValue() > 1)
        .map(Map.Entry::getKey)
        .sorted(Comparator.comparing(String::length))
        .collect(Collectors.toList());
  }
}